Products = new Mongo.Collection("products");
Barcodes = new Mongo.Collection("barcodes");
TempOrder = new Mongo.Collection("tempOrder");
Orders = new Mongo.Collection("orders");
Product_Order = new Mongo.Collection("product_order");

/*FS.debug = true;*/

Images = new FS.Collection("images", {
    stores: [new FS.Store.FileSystem("images", {path: "~/uploads"})]
});

Images.allow({
    insert: function () {
        return true;
    },
    update: function () {
        return true;
    },
    remove: function () {
        return true;
    },
    download: function () {
        return true;
    }
});
