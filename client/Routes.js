/**
 * Created by Rajith Hasith on 19/01/2015.
 */

Router.configure({
    notFoundTemplate: "PageNotFound"
});

Router.onBeforeAction(function () {
    // all properties available in the route function
    // are also available here such as this.params

    if (!Meteor.userId()) {
        // if the user is not logged in, render the Login template

        this.render('Login');
    } else {
        // otherwise don't hold up the rest of hooks or our route/action function
        // from running
        this.next();
    }
});

Router.plugin('dataNotFound', {notFoundTemplate: 'ProductNotFound'});

Router.map(function() {
    this.route('/', function () {
        //this.render('Home');
        this.render('Home');

    });

    this.route('/Add-Product', function () {
        this.render('AddStock');

    });

    this.route('/Test', function () {
        this.render('Test');

    });

    this.route('/Product/Product_Not_Found', function() {
        this.render('ProductNotFound');
    });

    this.route('ProductFull', {
        path: '/Product/:_id',
        data: function() {
            isEditing = false;
            return Products.findOne({_id: this.params._id});
        }
    });

    /*this.route('/Product/:_id', function () {
        var product = Products.findOne({_id: this.params._id});
        this.render('ProductFull', {data: product});
    });*/

    this.route('/Ordering', function () {
        this.render('Ordering');
    });

    this.route('/Ordering/Search/:id', function () {
        this.render('Ordering');
    });

    this.route('/Ordering/History', function(){
       this.render('OrderHistory');
    });

    this.route('/Ordering/History/:id', function(){
        this.render('OrderDetails');
    });

    this.route('/Ordering/FullOrder', function () {
        this.render('Ordering');
    });

    this.route('/Monitor-App', function () {
        this.render('AppMonitor');
    });



    this.route("notFound",{
        path:"*",
        template:"ProductNotFound"
    });

});


