/**
 *
 *This JavaScript file contains all the Client side code.
 * Created by Rajith Hasith Nandadasa on 06/04/2015.
 */
{

    /*
    * Subscribing for collections that published in server side
    * */
    Meteor.subscribe("products");
    Meteor.subscribe("barcodes");
    Meteor.subscribe("tempOrder");
    Meteor.subscribe("orders");
    Meteor.subscribe("product_order");
    Meteor.subscribe("images");

    var isEditing = false;
    var oldOrderVal;
    var deletingProduct;
    var tierValue = 0;
    var onShelf = false;
    var barcodeExist = false;


    /*
    * The View All Products Template Helper Function.
    * */
    Template.ViewProducts.helpers({
        /*
        * This function reruns all the products with their Images
        * */
        getProducts: function () {
            //deletingProduct = null;
            var Product = Products.find({}, {"sort": [['name', 'asc']]});

            var productList = [];

            Product.forEach(function (row) {
                var id = row._id;
                var name = row.name;
                var size = row.size;
                var price = row.price;
                pictureId = row.picture;
                var picture = Images.findOne({_id: pictureId});

                var product = {id: id, name:name, size:size, price:price, picture: picture}

                productList.push(product);
            });

            return productList;
        }

    });

    /*
    * The Navigation Bar Template Helpers
    * */
    Template.Nav.helpers({
        settings: function(){
            return{
                position:"bottom",
                limit: 10,
                rules:[
                    {
                        collection:Products,
                        field: 'name',
                        options: 'i',
                        template:Template.SearchPill,
                        noMatchTemplate: Template.NoMatch
                    }
                ]
            };
        }
    });

    /*
    * Navigation Bar Events fucntion.
    * */
    Template.Nav.events({
        "autocompleteselect .searchText": function(event, template, doc) {
            console.log("selected ", doc);
            Router.go('/Product/'+ doc._id);
            var test = document.getElementById('searchText');
            test.value = doc.name;
        }
    });


    /*
    * The Add Product Template Events function
    * */
    Template.AddStock.events({

        /*
        * The Add Product Button click event function
        * @param {event} e
        * */
        "click .add_btn": function (e) {

            var barcode = document.getElementById('productBarcode');
            var name = document.getElementById('productName');
            var size = document.getElementById('productSize');
            var price = document.getElementById('productPrice');
            var defaulOrder = document.getElementById('input');
            var leftPos = document.getElementById('leftPos').value;
            var colNo = document.getElementById('colNo').value;

            if (/\S/.test(barcode.value) && isNumber(barcode.value) && !barcodeExist ) {
                barcode.style.backgroundColor = "transparent";
                if (/\S/.test(name.value)) {
                    name.style.backgroundColor = "transparent";
                    if (/\S/.test(size.value)) {
                        size.style.backgroundColor = "transparent";
                        if (/\S/.test(price.value)) {
                            price.style.backgroundColor = "transparent";

                            var hashKey = name.value + size.value + price.value + Date.now();
                            var id = CryptoJS.MD5(hashKey).toString();

                            if (!/\S/.test(leftPos)){
                                leftPos = 0;
                            }
                            else{
                                leftPos = parseInt(leftPos);
                            }

                            if (!/\S/.test(colNo)){
                                colNo = 0;
                            }
                            else{
                                colNo = parseInt(colNo);
                            }


                            var picture = document.getElementById('addImage').files[0];
                            // var pictureObj = Images.insert(picture);
                            var imageID = "";
                            if(picture) {
                                var pictureObj = Images.insert(picture, function (err, pictureObj) {
                                    // Inserted new doc with ID fileObj._id, and kicked off the data upload using HTTP
                                    if (err) {
                                        throw err;
                                    }
                                    else {
                                        console.log("Image added successfully");
                                    }
                                });

                                imageID = pictureObj._id;
                                document.getElementById('addImage').value = "";
                            }



                            Meteor.call("addProduct", id, parseInt(barcode.value), name.value, size.value, price.value,onShelf, parseInt(tierValue), leftPos, colNo,  parseInt(defaulOrder.value), imageID);
                            tierValue = 0;
                            onShelf = false;

                            Router.go('/Product/' + id)
                        } else {
                            //Price empty
                            price.style.backgroundColor = "#F08080";
                        }
                    } else {
                        //size empty

                        size.style.backgroundColor = "#F08080";
                    }
                } else {
                    //name empty
                    name.style.backgroundColor = "#F08080";
                }
            } else {
                //barcod empty
                barcode.style.backgroundColor = "#F08080";
            }
        },

        /*
         * The On Shelf Checkbox click event function
         * @param {event} e
         * */
        'click .onShelfVal': function(e){
            var value = e.currentTarget.checked;
            onShelf = value;

            if(value){
                document.getElementById('enterPos').style.display = "inline";
            }else{
                document.getElementById('enterPos').style.display = "none";
            }
        },

        /*
         * The Tier Value Dropdown list click event function
         * @param {event} e
         * */
        'click .tierVal li': function(e){
            var tierVal = e.currentTarget.value;

            var tierTxt = document.getElementById("tierTxt");
            tierTxt.style.display = "inline";
            tierTxt.innerText = "On Tier " +tierVal;
            tierValue = tierVal;
            document.getElementById('tierBtn').innerHTML = "Tier "+ tierValue +" <span class="+"\""+"caret"+"\""+"><"+"/span>";


        },

        /*
         * The Left Position text box Key down event function
         * @param {event} e
         * */
        'keydown .leftPos':function(e) {
            var passKeys = [46, 8, 9, 27, 13];

            // Allow: backspace, delete, tab, escape and enter
            if (passKeys.indexOf(e.keyCode) !== -1 ||
                    // Allow: Ctrl+A
                (e.keyCode == 65 && e.ctrlKey === true) ||
                    // Allow: home, end, left, right
                (e.keyCode >= 35 && e.keyCode <= 39)) {
                // let it happen, don't do anything
                return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        },

        /*
         * The Column Number text box Key down event function
         * @param {event} e
         * */
        'keydown .colNo':function(e) {
            var passKeys = [46, 8, 9, 27, 13];

            // Allow: backspace, delete, tab, escape and enter
            if (passKeys.indexOf(e.keyCode) !== -1 ||
                    // Allow: Ctrl+A
                (e.keyCode == 65 && e.ctrlKey === true) ||
                    // Allow: home, end, left, right
                (e.keyCode >= 35 && e.keyCode <= 39)) {
                // let it happen, don't do anything
                return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        },

        /*
         * The Add image File input file change event function
         * @param {event} e
         * */
        'change .addImage': function(e) {
            var  file = e.target.files[0];
            readImage(file, "addImage", "newPicture");

        },

        /*
         * The Barcode text box Key up event function
         * @param {event} e
         * */
        "keyup .barcodeTxt": function (e) {
            var value = e.target.value;
            var BreakExeption = {};
            if (/\S/.test(value)){
                e.currentTarget.style.backgroundColor = "transparent";
                if (isNumber(value)) {
                    e.currentTarget.style.backgroundColor = "transparent";
                    document.getElementById("barcode_exists").style.display = 'none';
                    e.currentTarget.style.borderColor = "#cccccc";
                    var barcodes = Barcodes.find({});
                    try {
                        barcodes.forEach(function (b) {
                            if (b.barcode.toString().search(value) == 0) {
                                e.currentTarget.style.borderColor = "#F08080";
                                document.getElementById("barcode_exists").style.display = 'inline';
                                barcodeExist = true;
                                throw BreakExeption;
                            }
                            else {
                                e.currentTarget.style.borderColor = "#cccccc";
                                document.getElementById("barcode_exists").style.display = 'none';
                                barcodeExist = false;
                            }
                        });
                    }
                    catch (e) {
                        if (e !== BreakException) throw e;
                    }


                } else {
                    e.currentTarget.style.backgroundColor = "#F08080";
                    e.currentTarget.style.borderColor = "#cccccc";
                    document.getElementById("barcode_exists").style.display = 'none';
                    barcodeExist = false;
                }
            }else {
                e.currentTarget.style.backgroundColor = "transparent";
                e.currentTarget.style.borderColor = "#cccccc";
                document.getElementById("barcode_exists").style.display = 'none';
                barcodeExist = false;
            }

        },

        /*
         * The Product Name text box Key down event function
         * @param {event} e
         * */
        "keyup .nameTxt": function (e) {

            if ((/\S/.test(e.target.value))) {
                e.currentTarget.style.backgroundColor = "transparent";
            }
        },

        /*
         * The Product size text box Key down event function
         * @param {event} e
         * */
        "keyup .sizeTxt": function (e) {

            if ((/\S/.test(e.target.value))) {
                e.currentTarget.style.backgroundColor = "transparent";
            }
        },

        /*
         * The Product price text box Key down event function
         * @param {event} e
         * */
        "keyup .priceTxt": function (e) {

            if ((/\S/.test(e.target.value))) {
                e.currentTarget.style.backgroundColor = "transparent";
            }
        },

        /*
         * The product order default quantity buttons event function
         * @param {event} e
         * */
        "click .btn-number": function (e) {
            e.preventDefault();

            fieldName = e.currentTarget.attributes.getNamedItem('data-field').value;
            type = e.currentTarget.attributes.getNamedItem('data-type').value;
            var input = document.getElementById('input');
            var currentVal = parseInt(input.value);
            if (!isNaN(currentVal)) {
                if (type == 'minus') {

                    if (currentVal > input.getAttribute('min')) {
                        input.value = currentVal - 1;

                        //Meteor.call("updateTempOrder",this._id, parseInt(input.value));


                        e.currentTarget.disabled = false;
                        document.getElementById('plus').disabled = false;
                    }
                    if (parseInt(input.value) == input.getAttribute('min')) {
                        e.currentTarget.disabled = true;
                    }

                } else if (type == 'plus') {

                    if (currentVal < input.getAttribute('max')) {
                        input.value = currentVal + 1;

                        // Meteor.call("updateTempOrder",this._id, parseInt(input.value));

                        e.currentTarget.disabled = false;
                        document.getElementById('minus').disabled = false;
                    }
                    if (parseInt(input.value) == input.getAttribute('max')) {
                        e.currentTarget.disabled = true;
                    }

                }
            } else {
                input.value = 0;
            }

        },

        /*
         * The product order default quantity text box Key down event function
         * @param {event} e
         * */
        "keydown .input-number": function (e) {
            var passKeys = [46, 8, 9, 27, 13];

            // Allow: backspace, delete, tab, escape and enter
            if (passKeys.indexOf(e.keyCode) !== -1 ||
                    // Allow: Ctrl+A
                (e.keyCode == 65 && e.ctrlKey === true) ||
                    // Allow: home, end, left, right
                (e.keyCode >= 35 && e.keyCode <= 39)) {
                // let it happen, don't do anything
                return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        },

        /*
         * The product order default quantity text box forcus event function
         * @param {event} e
         * */
        "focus .input-number": function (e) {
            oldOrderVal = parseInt(e.currentTarget.value);
        },

        /*
         * The product order default quantity text box change event function
         * @param {event} e
         * */
        "change .input-number": function (e) {
            minValue = parseInt(e.currentTarget.attributes.getNamedItem('min').value);
            maxValue = parseInt(e.currentTarget.attributes.getNamedItem('max').value);
            valueCurrent = parseInt(e.currentTarget.value);
            // name = $(this).attr('name');
            if (valueCurrent >= minValue) {
                document.getElementById('minus').disabled = false;
            } else {
                // alert('Sorry, the minimum value was reached');
                e.currentTarget.value = oldOrderVal;
            }
            if (valueCurrent <= maxValue) {
                document.getElementById('plus').disabled = false;
            } else {
                //alert('Sorry, the maximum value was reached');
                e.currentTarget.value = oldOrderVal;
            }
        }


    });

    /*
    * The Product data Template Events function,
    * */
    Template.Product.events({
        "click .moreInfo": function (e) {
            var id = this.id;
            isEditing = false;
            Router.go('/Product/' + id)

        }
    });

    /*
     * The Product full information Template helpers function,
     * */
    Template.ProductFull.helpers({
        getImage: function(){
            var id = "";
            url = Router.current();
            id = url.params._id;
            var product = Products.findOne({_id: id});
            var imageID = product.picture;
            var Image = Images.findOne({_id: imageID});
            var n = Image.url;
            return Image;
        },

        getBarcodes: function () {
            var id = Router.current().params._id;
            var barcodes = Barcodes.find({productID: id});
            return barcodes;

        }
    });

    /*
     * The Product full information Template Event function,
     * */
    Template.ProductFull.events({
        "click .editInfo": function (e) {

            var id = Router.current().params._id;

            if (!isEditing) {

                var test = document.getElementById('edit_btn').childNodes.item(0);

                //document.getElementById('edit_btn').childNodes.item(0).classList.remove("glyphicon-edit");
                document.getElementById('add_barcode_rw').style.display = "";
                document.getElementById('item_name_rw').style.display = "";

                document.getElementById('size_txt').readOnly = false;
                document.getElementById('size_txt').style.borderStyle = "dotted";


                document.getElementById('price_txt').readOnly = false;
                document.getElementById('price_txt').style.borderStyle = "dotted";

                document.getElementById('defOrderQuant_txt').style.display = "none";
                document.getElementById('editOrderQuant').style.display = "inline";

                document.getElementById('image').style.display = "inline";

                document.getElementById('posTxt').style.display = "none";
                document.getElementById('editPos').style.display = "inline";

                document.getElementById('barcodeList_edit').style.display  ="inline";
                document.getElementById('barcodeList_normal').style.display  ="none";


                document.getElementById('del_btn').style.display = "inline";

                document.getElementById('edit_btn').className = "btn btn-success editInfo"
                document.getElementById('edit_btn').innerHTML = "<span class=\"" + "glyphicon glyphicon-ok\"" + " id=\"" + "icon\"" + "></span> Update";
                isEditing = true;
            } else if (isEditing == true) {

                var name = document.getElementById('name_txt');
                var size = document.getElementById('size_txt');
                var price = document.getElementById('price_txt');
                var defaultOrderQuant = document.getElementById('defOrderVal').value;
                var onShelf = document.getElementById('onShelfVal').checked;
                var leftPos = document.getElementById('leftPos').value;
                var colNo = document.getElementById('colNo').value;
                if (/\S/.test(name.value)) {
                    name.style.backgroundColor = "transparent";
                    if (/\S/.test(size.value)) {
                        size.style.backgroundColor = "transparent";
                        if (/\S/.test(price.value)) {
                            price.style.backgroundColor = "transparent";

                            if (onShelf) {
                                if (!/\S/.test(leftPos)) {
                                    leftPos = 0;
                                }
                                else {
                                    leftPos = parseInt(leftPos);
                                }

                                if (!/\S/.test(colNo)) {
                                    colNo = 0;
                                }
                                else {
                                    colNo = parseInt(colNo);
                                }
                            }
                            else {
                                tierValue = 0;
                                leftPos = 0;
                                colNo = 0;
                            }

                            var picture = document.getElementById('image').files[0];
                            // var pictureObj = Images.insert(picture);
                            var imageID = Products.findOne({_id: id}).picture;
                            if (picture) {
                                var pictureObj = Images.insert(picture, function (err, pictureObj) {
                                    // Inserted new doc with ID fileObj._id, and kicked off the data upload using HTTP
                                    if (err) {
                                        throw err;
                                    }
                                    else {
                                        console.log("Image added successfully");
                                    }
                                });

                                Images.remove({_id: imageID}, function (err) {
                                    if (err) {
                                        throw err;
                                    }
                                    else {
                                        console.log("Old Image removed successfully")
                                    }
                                });

                                imageID = pictureObj._id;
                                document.getElementById('image').value = "";
                            }

                            Meteor.call("updateProduct", id, name.value, size.value, price.value, onShelf, parseInt(tierValue), leftPos, colNo, parseInt(defaultOrderQuant), imageID);

                            document.getElementById('add_barcode_rw').style.display = "none";
                            document.getElementById('item_name_rw').style.display = "none";

                            document.getElementById('size_txt').readOnly = true;
                            document.getElementById('size_txt').style.borderStyle = "none";

                            document.getElementById('price_txt').readOnly = true;
                            document.getElementById('price_txt').style.borderStyle = "none";

                            document.getElementById('defOrderQuant_txt').style.display = "inline";
                            document.getElementById('editOrderQuant').style.display = "none";

                            document.getElementById('posTxt').style.display = "inline";
                            document.getElementById('editPos').style.display = "none";

                            document.getElementById('image').style.display = "none";

                            document.getElementById('barcodeList_edit').style.display  ="none";
                            document.getElementById('barcodeList_normal').style.display  ="inline";

                            document.getElementById('del_btn').style.display = "none";

                            document.getElementById('edit_btn').className = "btn btn-primary editInfo"
                            document.getElementById('edit_btn').innerHTML = "<span class=\"" + "glyphicon glyphicon-edit\"" + " id=\"" + "icon\"" + "></span> Edit Product Information";
                            document.getElementById("del_barcode_btn").style.display = 'none';
                            document.getElementById("multiSelect").style.display = 'none';
                            isEditing = false;

                        } else {
                            //Price empty
                            price.style.backgroundColor = "#F08080";
                        }
                    } else {
                        //size empty

                        size.style.backgroundColor = "#F08080";
                    }
                } else {
                    //name empty
                    name.style.backgroundColor = "#F08080";
                }
            }
        },

        "keyup .barcodeTxt": function (e) {
            var value = e.target.value;
            var BreakExeption = {};
            if (/\S/.test(value)){
                e.currentTarget.style.backgroundColor = "transparent";
                if (isNumber(value)) {
                    e.currentTarget.style.backgroundColor = "transparent";
                    document.getElementById("barcode_exists").style.display = 'none';
                    e.currentTarget.style.borderColor = "#cccccc";
                    var barcodes = Barcodes.find({});
                    try {
                        barcodes.forEach(function (b) {
                            if (b.barcode.toString().search(value) == 0) {
                                e.currentTarget.style.borderColor = "#F08080";
                                document.getElementById("barcode_exists").style.display = 'inline';
                                barcodeExist = true;
                                throw BreakExeption;
                            }
                            else {
                                e.currentTarget.style.borderColor = "#cccccc";
                                document.getElementById("barcode_exists").style.display = 'none';
                                barcodeExist = false;
                            }
                        });
                    }
                    catch (e) {
                        if (e !== BreakException) throw e;
                    }


                } else {
                    e.currentTarget.style.backgroundColor = "#F08080";
                    e.currentTarget.style.borderColor = "#cccccc";
                    document.getElementById("barcode_exists").style.display = 'none';
                    barcodeExist = false;
                }
            }else {
                e.currentTarget.style.backgroundColor = "transparent";
                e.currentTarget.style.borderColor = "#cccccc";
                document.getElementById("barcode_exists").style.display = 'none';
                barcodeExist = false;
            }

        },

        'change .barcodeList':function(e){
            if(isEditing) {
                var barcodeList = e.currentTarget;
                if(barcodeList.selectedOptions.length==1){
                    document.getElementById("multiSelect").style.display = 'none';
                    document.getElementById("del_barcode_btn").style.display = 'inline';
                    var barcode_id = barcodeList.options[barcodeList.selectedIndex].value;
                    var barcode = barcodeList.options[barcodeList.selectedIndex].text;
                }
                else{
                    document.getElementById("multiSelect").style.display = 'inline';
                    document.getElementById("del_barcode_btn").style.display = 'none';
                }

            }
        },

        'click .del_barcode':function(e){

            var barcodeList = document.getElementById('barcodeList_edit');
            var barcode_id = barcodeList.options[barcodeList.selectedIndex].value;
            var barcode = barcodeList.options[barcodeList.selectedIndex].text;
            Meteor.call("deleteBarcode",barcode_id);



        },


        'click .onShelfVal': function(e){
            var value = e.currentTarget.checked;

            if(value){
                document.getElementById('enterPos').style.display = "inline";
            }else{
                document.getElementById('enterPos').style.display = "none";
            }
        },

        'click .tierVal li': function(e){
            var tierVal = e.currentTarget;
            tierValue = tierVal.value;
            document.getElementById('tierBtn').innerHTML = "Tier "+ tierValue +" <span class="+"\""+"caret"+"\""+"><"+"/span>";




        },

        "click .delProduct": function (e) {
            Router.go('/Product/'+id+'/Deleted');
            var id = "";
            id = Router.current().params._id;
            deletingProduct = Products.findOne({_id: id});
            Meteor.call("deleteProductBarcode", id);
            Products.remove({_id: id});

            Images.remove({_id: deletingProduct.picture}, function(err){
                if(err){
                    throw err;
                }
                else{
                    console.log("Old Image removed successfully")
                }
            })

            var bla = 1;
            Router.route('/Product/:_id/Deleted', function () {
                this.layout('DeleteSuccess', {
                    // set a data context for the whole layout
                    data: {
                        name: deletingProduct.name,
                        Price: deletingProduct.price,
                        size: deletingProduct.size
                    }
                });
            });
        },

        "click .add_barcode": function (e) {
            var displayRw = document.getElementById('add_barcode_rw').style.display;
            var productId = Router.current().params._id;
            if (displayRw == 'none') {
                document.getElementById('add_barcode_rw').style.display = "";
            }
            else {
                var barcode = document.getElementById('new_barcode_txt').value;
                if (isNumber(barcode)&&!barcodeExist) {

                    Meteor.call("addBarcode", productId, barcode);
                    document.getElementById('new_barcode_txt').value = "";
                }

            }
        },

        'change .image': function(e) {
            var  file = e.target.files[0];
            readImage(file, "image", "picture");

        },

        "click .btn-number": function (e) {
            e.preventDefault();

            fieldName = e.currentTarget.attributes.getNamedItem('data-field').value;
            type = e.currentTarget.attributes.getNamedItem('data-type').value;
            var input = document.getElementById('defOrderVal');
            var currentVal = parseInt(input.value);
            if (!isNaN(currentVal)) {
                if (type == 'minus') {

                    if (currentVal > input.getAttribute('min')) {
                        input.value = currentVal - 1;

                        //Meteor.call("updateTempOrder",this._id, parseInt(input.value));


                        e.currentTarget.disabled = false;
                        document.getElementById('plus').disabled = false;
                    }
                    if (parseInt(input.value) == input.getAttribute('min')) {
                        e.currentTarget.disabled = true;
                    }

                } else if (type == 'plus') {

                    if (currentVal < input.getAttribute('max')) {
                        input.value = currentVal + 1;

                        // Meteor.call("updateTempOrder",this._id, parseInt(input.value));

                        e.currentTarget.disabled = false;
                        document.getElementById('minus').disabled = false;
                    }
                    if (parseInt(input.value) == input.getAttribute('max')) {
                        e.currentTarget.disabled = true;
                    }

                }
            } else {
                input.value = 0;
            }

        },

        "keydown .input-number": function (e) {
            var passKeys = [46, 8, 9, 27, 13];

            // Allow: backspace, delete, tab, escape and enter
            if (passKeys.indexOf(e.keyCode) !== -1 ||
                    // Allow: Ctrl+A
                (e.keyCode == 65 && e.ctrlKey === true) ||
                    // Allow: home, end, left, right
                (e.keyCode >= 35 && e.keyCode <= 39)) {
                // let it happen, don't do anything
                return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        },

        "focus .input-number": function (e) {
            oldOrderVal = parseInt(e.currentTarget.value);
        },

        "change .input-number": function (e) {
            minValue = parseInt(e.currentTarget.attributes.getNamedItem('min').value);
            maxValue = parseInt(e.currentTarget.attributes.getNamedItem('max').value);
            valueCurrent = parseInt(e.currentTarget.value);
            // name = $(this).attr('name');
            if (valueCurrent >= minValue) {
                document.getElementById('minus').disabled = false;
            } else {
                // alert('Sorry, the minimum value was reached');
                e.currentTarget.value = oldOrderVal;
            }
            if (valueCurrent <= maxValue) {
                document.getElementById('plus').disabled = false;
            } else {
                //alert('Sorry, the maximum value was reached');
                e.currentTarget.value = oldOrderVal;
            }
        }

    });

    Template.DeleteSuccess.helpers({
        test:function() {
            var text = Router.current();
        }
    });

    Template.Ordering.helpers({
        isFullList: function () {
            //deletingProduct = null;
            var urlSplit = Router.current().url.split('/');
            if (urlSplit[urlSplit.length - 1] == "FullOrder") {
                return true;
            }
            else
                return false;
        },

        settings: function(){
            return{
                position:"bottom",
                limit: 10,
                rules:[
                    {
                        collection:Products,
                        field: 'name',
                        options: 'i',
                        template:Template.SearchPill,
                        noMatchTemplate: Template.NoMatch
                    }
                ]
            };
        }
    });

    Template.Ordering.events({
        "click .fullOrderList": function (e) {
            var date = new Date().getTime();
            console.log("time val ", date);
            Router.go('/Ordering/FullOrder');
        },

        "autocompleteselect .orderSearchText":
            function(event, template, doc) {
            console.log("selected ", doc);
            event.currentTarget.value = "";
            Router.go('/Ordering/Search/' + doc._id);
        }
    });

    Template.SrearchOrder.helpers({
        searchOrdrProd: function () {


            var id = Router.current().params.id;
            var searchProduct = getQueryVariable("OrderProduct");

            return Products.find({_id: id});
        },

        getProductName: function () {
            var searchProduct = getQueryVariable("OrderProduct");
            var id = Router.current().params.id;
            return Products.findOne({_id: id}).name;
            // return searchProduct;
        }
    });

    Template.OrderProduct.events({

        "click .btn-number": function (e) {
            e.preventDefault();

            fieldName = e.currentTarget.attributes.getNamedItem('data-field').value;
            type = e.currentTarget.attributes.getNamedItem('data-type').value;
            var input = document.getElementById(this._id + 'input');
            var currentVal = parseInt(input.value);
            if (!isNaN(currentVal)) {
                if (type == 'minus') {

                    if (currentVal > input.getAttribute('min')) {
                        input.value = currentVal - 1;

                        Meteor.call("updateTempOrder", this._id, parseInt(input.value));


                        document.getElementById(this._id + 'plus').disabled = false;
                    }
                    if (parseInt(input.value) == input.getAttribute('min')) {
                        e.currentTarget.disabled = true;
                    }

                } else if (type == 'plus') {

                    if (currentVal < input.getAttribute('max')) {
                        input.value = currentVal + 1;

                        Meteor.call("updateTempOrder", this._id, parseInt(input.value));

                        document.getElementById(this._id + 'minus').disabled = false;
                    }
                    if (parseInt(input.value) == input.getAttribute('max')) {
                        e.currentTarget.disabled = true;
                    }

                }
            } else {
                input.value = 0;
            }

        },

        "keydown .input-number": function (e) {
            var passKeys = [46, 8, 9, 27, 13];

            // Allow: backspace, delete, tab, escape and enter
            if (passKeys.indexOf(e.keyCode) !== -1 ||
                    // Allow: Ctrl+A
                (e.keyCode == 65 && e.ctrlKey === true) ||
                    // Allow: home, end, left, right
                (e.keyCode >= 35 && e.keyCode <= 39)) {
                // let it happen, don't do anything
                return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        },

        "focus .input-number": function (e) {
            oldOrderVal = parseInt(e.currentTarget.value);
        },

        "change .input-number": function (e) {
            minValue = parseInt(e.currentTarget.attributes.getNamedItem('min').value);
            maxValue = parseInt(e.currentTarget.attributes.getNamedItem('max').value);
            valueCurrent = parseInt(e.currentTarget.value);
            // name = $(this).attr('name');
            if (valueCurrent >= minValue) {
                document.getElementById(this._id + 'minus').disabled = false;
            } else {
                // alert('Sorry, the minimum value was reached');
                e.currentTarget.value = oldOrderVal;
            }
            if (valueCurrent <= maxValue) {
                document.getElementById(this._id + 'plus').disabled = false;
            } else {
                //alert('Sorry, the maximum value was reached');
                e.currentTarget.value = oldOrderVal;
            }
        }
    });

    Template.FullOrder.helpers({
        fullOrderList: function () {
            var list = Products.find({tmpOrderQuant: {$ne: 0}});
            var count = list.count();

            //if(count > 0){
            //    document.getElementById('genOrder').style.display = "inline";
            //}
            return list;
        }
    });

    Template.FullOrder.events({
        "click .genOrderText": function (e) {
            var barcodes = "";
            var orderList = Products.find({tmpOrderQuant: {$ne: 0}});

            var currentdate = new Date();


            var barcodeTime =zeroFront(currentdate.getDate()) + "/"
                + zeroFront((currentdate.getMonth()+1))  + "/"
                + zeroFront(currentdate.getFullYear().toString().substr(2,2)) + ","
                + zeroFront(currentdate.getHours()) + ":"
                + zeroFront(currentdate.getMinutes()) + ":"
                + zeroFront(currentdate.getSeconds());

            orderList.forEach(function (row) {
                var productBarcode = Barcodes.findOne({productID: row._id}).barcode;
                for (i = 0; i < row.tmpOrderQuant; i++)
                {
                    var barcodeLine = barcodeTime+",  ,"+ parseInt(productBarcode) +"\r\n";
                    barcodes += barcodeLine;
                }
            });
            url = getTextFile(barcodes);
            // Create link.
            a = document.createElement("a");
            // Set link on DOM.
            document.body.appendChild(a);
            // Set link's visibility.
            a.style = "display: none";
            // Set href on link.
            a.href = url;
            // Set file name on link.
            a.download = "BARCODES.txt";
            // Trigger click of link.
            a.click();

            var currentdate = new Date();
            var orderId = zeroFront(currentdate.getDate()) + ""
                + zeroFront((currentdate.getMonth()+1))  + ""
                + zeroFront(currentdate.getFullYear()) + ""
                + zeroFront(currentdate.getHours()) + ""
                + zeroFront(currentdate.getMinutes()) + ""
                + zeroFront(currentdate.getSeconds());

            var datetime =zeroFront(currentdate.getDate()) + "/"
                + zeroFront((currentdate.getMonth()+1))  + "/"
                + zeroFront(currentdate.getFullYear()) + "-"
                + zeroFront(currentdate.getHours()) + ":"
                + zeroFront(currentdate.getMinutes()) + ":"
                + zeroFront(currentdate.getSeconds());

            var a = 9;
            var b = zeroFront(a);

            Orders.insert({
                _id: orderId,
                userName :Meteor.user().username,
                date: datetime
            });

            orderList.forEach(function (row) {
                Product_Order.insert({
                    orderID: orderId,
                    productID: row._id,
                    quantity: row.tmpOrderQuant
                });
            });

            orderList.forEach(function (row) {
                Products.update({_id: row._id}, {
                    $set: {
                        tmpOrderQuant: 0
                    }
                });
            });
            Router.go('/Ordering/History/'+ orderId);
        }
    });


    Template.OrderHistory.helpers({
        getOrderHistory: function(){
            var test = Orders.find({}, {"sort": [['date', 'asc']]});
            return test

        }
    });

    Template.OrderDetails.helpers({
        getOrderDetails: function(){
            var id = Router.current().params.id;
            var test = Orders.findOne({_id: id});
            return test;
        },

        getOrderProducts: function() {
            var orderId = Router.current().params.id;
            var orderProducts = Product_Order.find({orderID: orderId})

            var OrderProductList = [];

            orderProducts.forEach(function (row) {
                productDetails = Products.findOne({_id: row.productID});
                var name = productDetails.name;
                var size = productDetails.size;
                var orderQuantity = row.quantity;

                var product = {name: name , size:size, quantity:orderQuantity};
                OrderProductList.push(product);
            });


            return OrderProductList;

        }

    });


    Template.SearchProduct.helpers({
        getData: function () {
            /*return Session.get('searchData');*/
            var searchKey = getQueryVariable("searchKey");
            //return Meteor.call("searchProducts", searchKey);
            //  document.getElementById('searchInput').value = searchKey;
            if(isNumber(searchKey)){
                var productId = Barcodes.findOne({barcode: parseInt(searchKey)}).productID;
                return Products.find({_id: productId})
            }else if(searchKey.length == 1){
                return;
            }
            else{
                var regExpSearch = new RegExp(searchKey,'i');
                return Products.find({name:regExpSearch});
            }
        }

    });

    Template.AppMonitor.helpers({
        StockFillingList: function(){
            return Products.find({needQuant: {$ne: 0}});
        }
    });

    Template.AppMonitor.events({
        'click .isOrderVal': function(e){
            var order = e.currentTarget.checked;
            var productId = e.currentTarget.value;
            Meteor.call("setOrderCheck",productId,order);
        }
    });

    Accounts.ui.config({
        passwordSignupFields: "USERNAME_ONLY"
    });
}

function zeroFront(number){
    var newNumber =0;
    if(number<10){
        newNumber = "0"+number;
    }else{
        newNumber = number;
    }
    return newNumber;


}


function getQueryVariable(variable) {
    var query = window.location.search.substring(1);
    var vars = query.replace(/\+/g, ' ').split("&");
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split("=");
        if (pair[0] == variable) {
            return decodeURIComponent(pair[1]);
        }
    }
}

function getTextFile(text){
    var textFile = null;
    var data = new Blob([text], { type: 'text/plain'});

    if (textFile !== null) {
        window.URL.revokeObjectURL(textFile);
    }

    textFile = window.URL.createObjectURL(data);

    return textFile;
}

function isNumber(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}

function readImage(file, inputID, picID) {

    var reader = new FileReader();
    var image  = new Image();

    reader.readAsDataURL(file);
    reader.onload = function(_file) {
        image.src    = _file.target.result;              // url.createObjectURL(file);
        image.onload = function() {
            document.getElementById(picID).src = this.src;
        };
        image.onerror= function() {
            alert('Invalid file type: '+ file.type + ' \nPlease select an Image');
            document.getElementById(inputID).value = "";
        };
    };
}