/**
 * All the Server side code Includes here
 * Created by Rajith Hasith on 31/03/2015.
 */


/*
* Publishing all the collections.
* */
Meteor.publish("images", function() {
    return Images.find();
});

Meteor.publish("products", function () {
    return Products.find();
});

Meteor.publish("barcodes", function(){
    return Barcodes.find();
});

Meteor.publish("tempOrder", function(){
    return TempOrder.find();
});

Meteor.publish("orders", function(){
    return Orders.find();
});

Meteor.publish("product_order", function(){
    return Product_Order.find();
});

/*
* Meteror Methosd function
* */
Meteor.methods({

    /*
    * this function adds a product products collection.
    * @param {String} id
    * @param {int} barcode
    * @param {String} name
    * @param {String} size
    * @param {String} price
    * @param {String} default order quantity
    * @param {String} picture ID
    * */
    addProduct: function (id, barcode, name, size, price,
                          onShelf, tierNo, leftPos, colNo,
                          defaultOrderQuant, picture) {
        Products.insert({
            _id: id,
            name: name,
            size: size,
            price: price,
            position: {
                onShelf: onShelf,
                tierNo: tierNo,
                leftPosition: leftPos,
                noOfCols: colNo
            },
            defaultOrderQuant: defaultOrderQuant,
            tmpOrderQuant:0,
            picture: picture,
            fillQuant:0,
            needQuant:0

        });
        Barcodes.insert({
            productID:id,
            barcode: barcode
        });
    },

    /*
    * This function adda barcode to the barcodes collection.
    * @param {string} product ID
    * @param {int} barcode
    * */
    addBarcode:function(productID, barcode)   {
        if (/\S/.test(productID)) {
            if (/\S/.test(barcode)){
                Barcodes.insert({
                    barcode:barcode,
                    productID:productID
                });
            }
        }
    },

    /*
     * this function updates a product products collection.
     * @param {String} id
     * @param {int} barcode
     * @param {String} name
     * @param {String} size
     * @param {String} price
     * @param {String} default order quantity
     * @param {String} picture ID
     * */
    updateProduct:function(id, name, size,
                           price, onShelf,
                           tierNo, leftPos,
                           colNo, defaultOrderQuant,
                           picture){
        if (/\S/.test(name)) {
            if (/\S/.test(size)) {
                if (/\S/.test(price)) {
                    Products.update({_id: id} ,{$set: {
                        name:name,
                        size:size,
                        price: price,
                        position: {
                            onShelf: onShelf,
                            tierNo: tierNo,
                            leftPosition: leftPos,
                            noOfCols: colNo
                        },
                        defaultOrderQuant: defaultOrderQuant,
                        picture: picture
                    }});
                }
            }
        }

    },

    /*
     * This function delete all the barcodes of a product.
     * @param {string} product ID
     * */
    deleteProductBarcode:function(productID){
        Barcodes.remove({productID: productID});
    },

    /*
     * This function update the temporary order quantity value of a product
     * @param {string} product ID
     * @param {int} temporary order quantity
     * */
    updateTempOrder:function(productID, quantity){
        Products.update({_id:productID},{$set:{
            tmpOrderQuant: quantity}});

    },

    /*
     * This function updates the product need quantity
     * @param {string} product ID
     * @param {int} Needing quantity
     * */
    updateProductNeed: function(productID, NeedQuant){
        Products.update({_id: productID},{
            $set:{
                needQuant: NeedQuant}});
    },

    updateProductFill: function(productID, FillQuant){
        Products.update({_id: productID},{
            $set:{
                fillQuant: FillQuant}});
    },

    /*
     * This function sets the product need quantity and fill quantity to 0.
     * */
    resetProductCounts: function(){
        Products.update({},{
            $set:{
                fillQuant:0,
                needQuant:0,
                Order: false
            }},{
            multi:true
        });
    },

    setOrderCheck: function(productID,isOrder){
        Products.update({_id: productID},{
            $set:{
                Order: isOrder
            }});
    },

    /*
     * This function delete a one barcode of a product
     * @param {string} barcode id
     * */
    deleteBarcode:function(id){
        Barcodes.remove({_id: id});
    }
});


